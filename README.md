The Heat Exchanger to Air Plenum Piping assembly connects the cold side outputs from a pair of heat exchangers to the air plenum. It consists of three pipe segments and six pipe fittings. It merges the flow that was split into the paired heat exchangers.

This sub-assembly is self-contained and does not require specifying any physical parameters.